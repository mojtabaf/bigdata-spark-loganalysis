# README #

### Folder Structure: ###

* .git: for development we used bitbucket, and our .git folder contains all information about our project repository.  
* log_analyzer.py: script for the second assignment which is related to analyzing log files with spark.
* log_analyzer: symbolic links for the ‘log_analyzer.py’ script in order to be able to run script without ‘.py’ extension for covering the assignment run command requirement. 
* .gitignore: contains the ignored path of the files that we don't want to track with source controller.
* iliad-anonymized-10: output of running question or query 9 for anonymizing the log files for iliad host.
* odyssey-anonymized-10: output of running question or query 9 for anonymizing the log files for odyssey host.
* iliad-anonymized-11: provided anonymized logs with professor for iliad host. 
* odyssey-anonymized-11: provided anonymized logs with professor for odyssey host.
* iliad: contains log files related to iliad host.
* odyssey: contains log files related to odyssey host.
* log.pdf: assignment in detail.

### How to configure your system:: ###

* Your system have to have python3
* sudo pip3 install ipython
* Add this lines in your ~/.bashrc (linux) or ~/.bash_profile (Mac)
       export SPARK_HOME=/usr/local/spark [your spark directory]
       export PATH=$PATH:$SPARK_HOME
       export PYSPARK_PYTHON=python3
       export PYSPARK_DRIVER_PYTHON=ipython
       export PYTHONPATH=$SPARK_HOME/python/:$PYTHONPATH
       export PYTHONPATH=$SPARK_HOME/python/lib/pyspark.zip:$PYTHONPATH
       export PYTHONPATH=$SPARK_HOME/python/lib/py4j-0.10.4-src.zip:$PYTHONPATH

* Save and close your ~/.bashrc (linux) or ~/.bash_profile (Mac).
* source ~/.bashrc(linux) or source ~/.bash_profile

### How to run program: ###
* Run this command: 
      ./log_analyzer  -q  <i>  <dir1>  <dir2>

      Where <dir1> and <dir2> are two directories containing log files collected on two 
      different hosts, and <i> is the question number.
### Requirements: ###
* python3
* ipython
* Correct setting of your standalone spark.