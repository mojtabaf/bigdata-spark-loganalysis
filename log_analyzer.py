#! /usr/bin/env python3
import sys
import os
import argparse
import ntpath
from pyspark import SparkContext, SparkConf
import re
import shutil


users = []


def get_message(line):
    """
    this method is responsible for returning the message part of each log in log files.
    :param line: log line in each log files
    :return: message part
    """
    n = 4
    # split line with space
    groups = line.split()
    # join the message part with space and return it.
    message = ' '.join(groups[n:])
    return message


def starter_sessions_pair(line):
    """
    this method accepts a message and extracts the user id then returns (user id, 1)
    :param line: message that contains 'Starting session'
    :return: user, 1
    """
    # split message with space
    line_elements = line.split(' ')
    # extract user id from message. The last token of message is user id.
    user = line_elements[len(line_elements) - 1].split(".")[0]
    # return user, 1 for be able to count how many times user id is repeated.
    return user, 1


def starter_sessions_users(line):
    """
    this method is responsible for retrieving the user from message
    :param line: message that contains 'started session' in log files.
    :return: user who started a session
    """
    # split message with space
    line_elements = line.split(' ')
    # find user name from message
    user = line_elements[len(line_elements) - 1].split(".")[0]
    return user


def is_starting_sessions(line):
    """
    this method is responsible for filtering the messages which is related to information
    that contains Started session for users.
    :param line: message string
    :return: True if message is related to started session.
    """
    # create a regular expression
    regexp = re.compile(r'Started Session \b\w+\b of user \b\w+\b')
    # if massage search returns true for regular expression
    if regexp.search(line):
        return True
    else:
        return False


def is_achille_starting_sessions(line):
    """
    this method is responsible for getting a message and returning true if it contains
    'Started Session [ID] of user achille' otherwise return false.
    :param line: message as a input
    :return: True if massage is 'Started Session [ID] of user achille'
    """
    # create a regular expression
    regexp = re.compile(r'Started Session \b\w+\b of user achille')
    # if massage search returns true for regular expression
    if regexp.search(line):
        return True
    else:
        return False


def is_contains_error(line):
    """
    this method is responsible for getting a message and returning true if it contains 'error' otherwise returns false.
    :param line: message as a input
    :return: True if massage is 'Started Session [ID] of user achille'
    """
    # it checks whether 'error' exists in the message or not
    if re.search('error', str(line), re.IGNORECASE):
        return True
    else:
        return False


def question_one(sc, folders_path):
    print('* Q1: line counts:')
    # iterate over logs folder which belongs to hosts
    for folder in folders_path:
        # load all text files in each folder
        files = sc.textFile(folder + '/*')
        # count the number of total line that we have in log files which is stored in for each host
        data = files.count()
        # print host name and the number of log line that we have for each host
        print('  + ' + ntpath.basename(folder) + ': ' + str(data))


def question_two(sc, folders_path):
    print('* Q2: sessions of user ’achille’')
    # iterate over logs folder which belongs to hosts
    for folder in folders_path:
        # load all text files in each folder
        files = sc.textFile(folder + '/*')
        # first, maps all lines to messages that exist in each line in log files. Then filters and keeps only messages
        # that contain Starting session with user 'achille'.
        data = files.map(get_message).filter(is_achille_starting_sessions)
        # count the number of messages that related to starting session with 'achille' username.
        data = data.count()
        # print host name and the number of log line that contain a message for starting session with 'achile' username.
        print('  + ' + ntpath.basename(folder) + ': ' + str(data))


def question_three(sc, folders_path):
    """
    this method is responsible for returning the list unique user names who started a session for each host.
    :param sc: spark context
    :param folders_path: folders of logs that are related to the selected hosts
    """
    print('* Q3: unique user names:')
    # iterate over logs folder which belongs to hosts
    for folder in folders_path:
        # load all text files in each folder
        files = sc.textFile(folder + '/*')
        # first, maps all lines to messages that exist in each line in log files related to each host.
        # then filters and keeps only messages that contain Starting session.
        data = files.map(get_message).filter(is_starting_sessions)
        # first, map all message that contains 'Starting session' to the list of their user.
        # Then uses distinct for users RDD in order to remove duplicated username.
        data = data.map(starter_sessions_users).distinct()
        # collect distinct user from each host logs
        data = data.collect()
        # print list of distinct user of host
        print('  + ' + ntpath.basename(folder) + ': ' + str(data))


def question_four(sc, folders_path):
    """
    this method is responsible for counting and listing the number of sessions started per user for each host
    :param sc: spark context
    :param folders_path: folders of logs that are related to the selected hosts
    """
    print('* Q4: sessions per user:')
    # iterate over logs folder which belong to hosts
    for folder in folders_path:
        files = sc.textFile(folder + '/*')
        # first, maps all lines to messages that exist in each line in log files related to each host.
        # Then filters and keeps only messages that contain Starting session.
        data = files.map(get_message).filter(is_starting_sessions)
        # map messages to (user1, 1), (user2, 1),user1, 1), (user1, 1), (user2, 1), ..... . Then we can reduce
        # key value pairs to (user1, 3),(user2, 2)...
        data = data.map(starter_sessions_pair).reduceByKey(lambda x, y: x + y)
        # collect the user and user occurrences who are started a session as a list
        data = data.collect()
        # print list of users and the number of sessions that started with them for each host
        print('  + ' + ntpath.basename(folder) + ': ' + str(data))


def question_five(sc, folders_path):
    """
    this method is responsible for returning the number of log messages that contain 'error'
    :param sc: spark context
    :param folders_path: folders of logs that are related to the selected hosts
    """
    print('* Q5: number of errors:')
    # iterate over logs folder which belong to hosts
    for folder in folders_path:
        # load all text files in each folder
        files = sc.textFile(folder + '/*')
        # first, maps all lines to messages that exist in each line in log files. Then filters and keeps only messages
        # that contain 'error'.
        data = files.map(get_message).filter(is_contains_error)
        # count the number of messages that related to error logs.
        data = data.count()
        print('  + ' + ntpath.basename(folder) + ': ' + str(data))


def question_six(sc, folders_path):
    """
    this method is responsible for retrieving the most five frequent error messages.
    :param sc: spark context
    :param folders_path: folders of logs that are related to the selected hosts
    """
    print('* Q6: 5 most frequent error messages:')
    # iterate over logs folder which belong to hosts
    for folder in folders_path:
        # load all text files in each folder
        files = sc.textFile(folder + '/*')
        # first, maps all lines to messages that exist in each line in log files. Then filters and keeps only messages
        # that contain 'error'. after that  we map them to ('error message', 1). now, we can reduce all messages
        # which have same content by summation of values. ('error1', 1), ('error1', 1), ('error2', 1),
        # ('error1', 1), ('error2', 1) => result: ('error1', 3), ('error2', 2)
        data = files.map(get_message).filter(is_contains_error).map(lambda x: (x, 1)).reduceByKey(lambda x, y: x + y)
        # map the content ('error1', 3), ('error2', 2) to (3, 'error1'), (2, 'error2') in order to be able to sort
        # error messages by their counts
        data = data.map(lambda x: (x[1], x[0])).sortByKey(ascending=False)
        #  print host name
        print('  + ' + ntpath.basename(folder) + ': ')
        # print five top most frequent error messages
        for error_message in data.take(5):
            print('    - ' + str(error_message))


def question_seven(sc, folders_path):
    """
    this method is responsible for printing users who started a session on both hosts 'iliad' and 'odyssey'
    :param sc: spark context
    :param folders_path: folders of logs that are related to the selected hosts
    """
    print('* Q7: users who started a session on both hosts, i.e., on exactly 2 hosts.')
    # load all log files for first host
    files_first = sc.textFile(folders_path[0] + '/*')
    # first, maps all lines to messages that exist in each line in log files related to the first host.
    # Then filters and keeps only messages that contain Starting session.
    files_first = files_first.map(get_message).filter(is_starting_sessions)
    # first, maps all message that contain 'Starting session' to the list of their user.
    # Then uses distinct for users RDD in order to remove duplicated username.
    first_rdd = files_first.map(starter_sessions_users).distinct()
    # load all log files for second host
    files_second = sc.textFile(folders_path[1] + '/*')
    # first, map all lines to messages that exist in each line in log files related to the second host.
    # then filter and keep only messages that contain Starting session. after that, map all message that contains
    # 'Starting session' to the list of their user.then use distinct for users RDD in order
    # to remove duplicated usernames.
    second_rdd = files_second.map(get_message).filter(is_starting_sessions).map(starter_sessions_users).distinct()
    # result will be the intersection of first host user list RDD to the second RDD
    #  that contains User list from second host
    result = first_rdd.intersection(second_rdd)
    # return the intersection of teo user list as a collection.
    result = result.collect()
    #  print the intersection of first and second hosts distinct users.
    print('  + : ' + str(result))


def question_eight(sc, folders_path):
    """
    this method is responsible for retrieving list of users who started a session on exactly one host, with host name.
    :param sc: spark context
    :param folders_path: folders of logs that are related to the selected hosts
    """
    print('* Q8: users who started a session on exactly one host, with host name.')
    # load all log files for first host
    files_first = sc.textFile(folders_path[0] + '/*')
    # first, map all lines to messages that exist in each line in log files related to the first host.
    # then filter and keep only messages that contain Starting session.
    first_rdd = files_first.map(get_message).filter(is_starting_sessions)
    # first, map all message that contains 'Starting session' to the list of their user.
    # then use distinct for users RDD in order to remove duplicated username.
    first_rdd = first_rdd.map(starter_sessions_users).distinct()
    # load all log files for second host
    files_second = sc.textFile(folders_path[1] + '/*')
    # now, we can map all lines to messages that exist in each line in log files related to the second host.
    # then filter and keep only messages that contain Starting session.after that we have to map all message that
    # contains 'Starting session' to the list of their user. finally, use distinct for users RDD
    # in order to remove duplicated username
    second_rdd = files_second.map(get_message).filter(is_starting_sessions).map(starter_sessions_users).distinct()
    # we can subtract list of distinct users that we have from second host from first host. then map user to the
    # (user, host_name) so we have [('gaia', 'iliad'), ('pollux', 'iliad'), ('helene', 'iliad')]
    result_fist = first_rdd.subtract(second_rdd).map(lambda x: (x, 'iliad'))
    # we can subtract list of distinct users that we have from first host from second host.then map user to the
    # (user, host_name) so we have [(’ares’, ’odyssey’)]
    result_second = second_rdd.subtract(first_rdd).map(lambda x: (x, 'odyssey'))
    # at this point we should union two lists that we have. so the result will be
    # [('gaia', 'iliad'), ('pollux', 'iliad'), ('helene', 'iliad'), (’ares’, ’odyssey’)]. Then we can convert the RDD to
    # a list with using collect method.
    result = result_fist.union(result_second).collect()
    # print the union result.
    print('  + : ' + str(result))


def replace_usernames(line):
    """
    this method is responsible for replacing the actual user id with anonymized user id
     which is mapped to actual user id. We sorted the mapping list in users variable which was defined in global scope
    :param line: message part of each log
    :return: anonymized message. (replace actual user id with anonymized user id in the message)
    """
    # iterate over users which contain user mapping tuples
    for item in users:
        # replace actual user which is item[0] with anonymized user id which is item[1]
        line = line.replace(item[0], item[1])
    # return anonymized message
    return line


def question_nine(sc, folders_path):
    """
    this method is responsible for anonymizing the log files related to each host. we have to replace the user id
    with new user id which follows user-0, user-1, ... pattern. The anonomized log files should save in new directory
    in same path with names like iliad-anonymized-10 , ...
    :param sc: spark context
    :param folders_path: folders of logs that are related to the selected hosts
    """
    print('* Q9: anonymizing the logs files:')
    for folder in folders_path:
        # get current directory path. The tail is the name host name and head is the directory path
        head, tail = os.path.split(folder)
        # print the host name
        print("  + " + tail + ":")
        # load all log files for first host
        files = sc.textFile(folder + '/*')
        # defined user variable as a global variable in order to have an access to it in outside of this method
        global users
        # first, maps all lines to messages that exist in each line in log files related to each host.
        # Then filters and keeps only messages that contain Starting session.
        # first, maps all message that contain 'Starting session' to the list of their user.
        # Then uses distinct for users RDD in order to remove duplicated username.
        users = files.map(get_message).filter(is_starting_sessions).map(starter_sessions_users).distinct()
        # at this point we have list of distinct users for each host, so we have to sort user list alphabetically.
        # then we have to keep their index in the RDD with zipWithIndex action.
        # so we have (user, user index in sorted list).
        # now we can map them to (userId, 'user-user index in sorted list')
        # finally, collect the RDD as a list
        users = users.sortBy(lambda x: x).zipWithIndex().map(lambda x: (x[0], 'user-' + str(x[1]))).collect()
        # print user name mapping for each host
        print('\t. User name mapping: ' + str(users))
        # create a new folder path for anonymized logs related to each host
        anonymized_folder_name = tail + '-anonymized-10'
        # print the name of anonimyzed directory for each host
        print(' \t. Anonymized files: ' + anonymized_folder_name)
        # in the RDD that contains what we have from the selected host log files, we should replace all actual user id
        # with anonymized user id which is mapped to that user in mapping list
        anonymized_files = files.map(replace_usernames)
        # check if anonymized directory exists in the path
        if os.path.isdir(head.split(':')[1] + "/" + anonymized_folder_name):
            # if it exists we should remove the directory otherwise we will receive the error while we want
            # to save anonymized RDD in to a files
            shutil.rmtree(head.split(':')[1] + "/" + anonymized_folder_name)
        # save anonymized RDD for each host in related new directory
        anonymized_files.saveAsTextFile(head + "/"+anonymized_folder_name)


def main():
    """
    usage: log_analyzer.py [-h] -q QUESTION Files [Files ...]
    this python script woks on the log file for log analyzing and answer some queries.
    required argument: -q or --question which is the query number.
    query number should be a number and it should be less than 10.
    required argument: host names that could be a list of host names which are separated by space.
    positional arguments:
        Files  you can enter list of file path for analyzing.
    optional arguments:
        -h, --help   show this help message and exit
    -q QUESTION, --question QUESTION
        you should enter the question or query number.
    """
    parser = argparse.ArgumentParser(description='log analyzer help:')
    # it defines positional arguments, Host positional argument for script, and its help message
    parser.add_argument('hosts', metavar='Hosts', type=str, nargs='+',
                        help='you can enter list of file path for analyzing.')
    # defines the query number arguments which should be answered, and the query number arguments help message
    parser.add_argument('-q', '--question', type=int,
                        help='you should enter the question or query number.', required=True)
    # parse script argument and find mistakes in calling script and return appropriate messages
    args = parser.parse_args()
    # if the range of query or question number is bigger that 9, application returns ERROR messages and shows help.
    if args.question and args.question > 9:
        print("ERROR: question number is in rage of 1 to 9")
        parser.print_help()
        sys.exit(0)
    # create spark configuration for this script. The spark job name will be 'log analyzer',
    # and this job will use all available cores in cpu in order to run maximum number of possible threads.
    conf = SparkConf().setMaster('local[*]').setAppName('log analyzer')
    # create spark context with configuration which is created in previous line.
    sc = SparkContext(conf=conf)
    #  it keeps all logs folder which we should work on.
    logs_path = []
    # append folders of logs which are related to hosts that are defined in script arguments
    for host in args.hosts:
        logs_path.append('file://' + os.path.abspath(host))
    # answer query 1 if user enters query number 1 after '-q'
    if args.question == 1:
        question_one(sc, logs_path)
    # answer query 2 if user enters query number 2 after '-q'
    elif args.question == 2:
        question_two(sc, logs_path)
    # answer query 3 if user enters query number 3 after '-q'
    elif args.question == 3:
        question_three(sc, logs_path)
    # answer query 4 if user enters query number 4 after '-q'
    elif args.question == 4:
        question_four(sc, logs_path)
    # answer query 5 if user enters query number 5 after '-q'
    elif args.question == 5:
        question_five(sc, logs_path)
    # answer query 6 if user enters query number 6 after '-q'
    elif args.question == 6:
        question_six(sc, logs_path)
    # answer query 7 if user enters query number 7 after '-q'
    elif args.question == 7:
        question_seven(sc, logs_path)
    # answer query 8 if user enters query number 8 after '-q'
    elif args.question == 8:
        question_eight(sc, logs_path)
    # answer query 9 if user enters query number 9 after '-q'
    elif args.question == 9:
        question_nine(sc, logs_path)
    # stop spark context and free resources.
    sc.stop()


if __name__ == "__main__":
    main()
